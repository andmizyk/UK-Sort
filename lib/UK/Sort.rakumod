unit module UK::Sort:ver<0.0.1>:auth<zef:andm>;

multi sub sortuk {()}
multi sub sortuk($item) { ($item,) }
multi sub sortuk(@data) is export {
	return sort { cmp_uk($^a,$^b) },@data
}

sub cmp_uk(Str $a, Str $b -->Order) is export {
	if $a eq $b { return Same }

	for $a.comb Z $b.comb {
		given ($_[0] coll $_[1]) {
		when Less { return Less }
		when More { return More }}
	}

	return $a.chars <=> $b.chars
}
