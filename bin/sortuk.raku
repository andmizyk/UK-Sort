#!/usr/bin/env raku
use v6.d;
my @data = @*ARGS;

if +@*ARGS == 1 && @*ARGS[0].IO.f { @data = @*ARGS[0].IO.lines }
else { @data = @*ARGS }

.put for sort { cmp_uk($^a,$^b) }, @data;

# =========================================================================

sub cmp_uk(Str $a, Str $b -->Order) is export {
	if $a eq $b { return Same }

	for $a.comb Z $b.comb {
		given ($_[0] coll $_[1]) {
			when Less { return Less }
			when More { return More }
		}
	}

	return $a.chars <=> $b.chars

}
