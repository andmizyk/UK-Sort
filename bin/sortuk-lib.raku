#!/usr/bin/env raku
use v6.d;
use lib 'lib';
use UK::Sort;

my @data = @*ARGS;

if +@*ARGS == 1 && @*ARGS[0].IO.f { @data = @*ARGS[0].IO.lines }
else { @data = @*ARGS }

.put for sortuk @data;
